mlflow_demo
==============================

This repo was created to demo how and why you would use mlflow in your ml development. It comes with a runbook to startup both a jupyter development environment with docker and a seperate docker for exposing and running a mlflow server.

Why MLFlow
----------

MLFlow according to the documentation;  
https://github.com/mlflow/mlflow/pkgs/container/mlflow

When we think about common hurdles when moving our ml experiments into a production environment the following concerns are common:  

1. Data quality and availability
2. Scalability and performance
3. Model interpretability and explainability
4. Model robustness and reliability
5. Ethical considerations and biases
6. Integration with existing systems and workflows
7. Monitoring and maintenance
8. Security and privacy

MLFlow has useful development patterns that address some parts of these points, especially when we think about 3, 4, 6 and 7

Running the demo
----------------

Requirements:
- docker

Make commands:

```bash
make build
make run
make train
make serve
make post
make stop
make rm
```

The make run command will start up the 2 containers `app` and `mlflow` and will expose the following ports:  
8888 - jupyter notebook
5000 - mlflow server

You can play with the training notebooks in the `notebooks` folder and see how the mlflow server is updated with the runs.

Or you can use the `make train` command to run the training script in the `mlflow_demo` folder. Following along the make commands for the various steps.

Project Organization
------------

    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    |
    ├── README.md          <- The top-level README for developers using this project.
    |
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docker
    │   ├── dev            <- Contains a Dockerfile for the dev environment.
    │   └── prod           <- Contains a Dockerfile for the prod environment.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    |
    ├── mlflow_demo                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module.
    │   ├── data           <- Scripts to download or generate data
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   ├── features       <- Scripts to turn raw data into features for modeling    
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations predictions.
    |
    └── tox.ini            <- tox file with settings for running tox; see tox.testrun.org


--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>

## Serving a model

from a container with access to the tracking server

```bash

mlflow models serve -m "models:/test_model_registry_1/Production" --no-conda

```

alternatively consult the docs to see how you can serve a model via sagemaker or azure ml

## Serving a model with a custom docker image

```bash
mlflow models build-docker -m "models:/test_model_registry_1/Production" -n "test_model_registry_1" --no-conda
```

## Posting data to a served model

It can be quite hard to find the exact documentation on how you can post data to the served models. Here are some examples with a link within the mlflow docs:  
https://www.mlflow.org/docs/latest/models.html#built-in-deployment-tools

here we are serving the model from the registry with the name `test_model_registry_1` and the stage `Production`

```python
import json
with open("sample.json", "w") as f:
    f.write(json.dumps(
        {
            "dataframe_split": test_post_df.to_dict(orient='split')
        }
    ))

```

```bash
!cat -A sample.json | curl http://0.0.0.0:8001/invocations \
                        --request POST \
                        --header 'Content-Type: application/json' \
                        --data-binary @-
```

where the url points to the location of the served model.

## Unit testing and other templates

Testing can be done for various aspects of a project.  
Here are some variants of testing aranged by when you might encounter them:  

- Unit Testing
- Integration Testing
- Functional Testing
- Smoke Testing
- Sanity Testing
- Regression Testing
- System Testing
- Performance Testing
- Stress Testing
- Usability Testing
- Security Testing
- Compatibility Testing
- Alpha Testing
- Beta Testing

There are many other types of tests and opinions and names can vary...

We will cover some templates for the first 4 types of testing in this project to help get people started.

System testing to beta testing are all more advanced forms of testing that we do towards the end of a project.

## Unit testing conventions

For more information see pages like https://learn.microsoft.com/en-us/dotnet/core/testing/unit-testing-best-practices

### Types of testing 

Typical folder structure

```
my_project/
|-- my_project/
|   |-- __init__.py
|   |-- main.py
|   |-- utils.py
|   |-- module_a/
|   |   |-- __init__.py
|   |   |-- module_a_func.py
|-- tests/
|   |-- resources/
|   |   |-- data_fixtures/
|   |   |   |-- data.json
|   |   |-- test_fixtures/
|   |   |   |-- test_data.json
|   |   |-- snapshots/
|   |-- module_a/
|   |   |-- test_utils.py
|   |-- test_main.py
|   |-- test_integration.py
|   |-- test_functional.py
|   |-- test_smoke.py
|   |-- test_sanity.py
|-- pyproject.toml
|-- requirements.txt
|-- README.md
|-- LICENSE
|-- Makefile
```

### pyprogram.toml

Making sure your modules are setup and installable is important. Sometimes you may prefer not adding the modules as an installable package, in that case you must make sure that the modules are discoverable in each files path so that they can be imported properly both when running the code and testing it.

### Naming tests

In python we always adopt the snake_case 

The name of your test should consist of three parts:

- starts with `test_` (pytest requirement)
- The name of the method being tested.  
- `given_` and the scenario under which it's being tested.  
- The expected behavior when the scenario is invoked.  

### Building tests

The init script in the `tests` folder generally imports the packages and code developed and used in the project, which allows for running the various tests.

### Parts of each test

Tests are generally broken up into sections:  

- Expectation and fakes [stubs and mocks]
- Declare vars 
- Act / run system
- Assert / verify expectation 

This way the unit tests are easy to follow and we avoid introducing space for bugs.

### Fixtures and Snapshots

In order to decouple the code being tested from the environment and data dependencies we often need to make fixtures and snapshots.

Fixtures can be made manually, for example by defining the json data structure and decorating it as a fixture for pytest;  

```
@pytest.fixture
def json_input():
    return {
        "name": "John Doe",
        "age": 30,
        "email": "johndoe@example.com",
        "phone": "+1-555-555-5555"
    }
```

If the fixtures are used elsewhere they should be stored in a reasonable place, for example;  
`tests/resources/data_fixtures`  

For more complicated dependencies like API requests we include examples for mocking the interaction using tools like the `responses` package;  

```
@responses.activate
def test_retrieve_tasks(to_do_list_fixture):
    """Test that the API returns the correct data"""
    # Declare vars
    api_uri = API_TODOS

    # Expectation and fakes
    responses.add(responses.GET, api_uri, json=to_do_list_fixture, status=200)
    data_fixture = TodoList([Todoitem(**dict_body) for dict_body in to_do_list_fixture])


    # Act or run system
    data_request_mock = requests.get(api_uri).json()
    request_body = TodoList([Todoitem(**dict_body) for dict_body in data_request_mock])

    # Assert or verify result
    assert data_fixture == request_body
```

For functions that depend on various data inputs we also include templates for automatically creating snapshots that act as fixtures.  
Snapshots are made automatically by adding the snapshot parameter during testing;  
`python3 -m pytest --snapshot-update`  

**ONLY USE THIS PARAMETER WHEN TAKING NEW SNAPSHOTS OF THE DATA!**

When making new snapshots the tests are generally gaurenteed to pass, make sure to test against the snapshots already made (do not make new ones every time)

Snapshot example;  

```
def test_process_dataframe_returns_expected_frame(snapshot, dataframe_input: pd.DataFrame):   
    data_frame = process_dataframe(dataframe_input)  
    snapshot.assert_match(data_frame.to_csv(index=False), 'unique_data_fixture_name')  
```

### Good quality

To achieve good quality we should try to achieve the following conditions;  

- Small, minimal tests  
- Explicit variables and objects (no special values or unexplained interactions)  
- Minimal logic  
    - If you need to apply logical steps consider refactoring the functions so that they are self explanatory and easilty testable  
- Each test should perform only 1 action  
- Create fakes for all dependencies
- Create stubs and fixtures for referenced variables

### Types of tests

Functional Testing:  
- Boundary Value: Testing the behavior of a system at the extreme limits of its input ranges.  
- Input Domain Testing: Examining a system's behavior by testing with various combinations of input values.  
- Equivalence Partitioning: Dividing the input domain into equivalent classes to reduce the number of test cases while maintaining test effectiveness.  
- Syntax Checking: Validating the format and structure of input data or code to ensure it follows predefined rules.  

Structural Testing:  
- Condition Testing: Verifying the behavior of a system by focusing on the conditions and decisions made within the code.  
- Path Testing: Examining the different execution paths within a system to ensure all possible code paths are tested.  
- Branch Testing: Ensuring that each branch of decision-making code is executed at least once.  
- Statement Testing: Testing a system by ensuring that each line or statement of the code is executed at least once.  
- Expression Testing: Evaluating the behavior of a system by focusing on the expressions used in the code and their different possible outcomes.  

Error-Based:  
- Mutation Testing: Assessing the quality of a test suite by introducing small, deliberate faults (mutations) in the code and checking if the test suite can detect them.  
- Fault Seeding: Intentionally inserting known defects into a system to evaluate the effectiveness of the testing process in finding those defects.  
- Historical Test Data: Using data from past testing efforts to inform and improve the current testing process.  
