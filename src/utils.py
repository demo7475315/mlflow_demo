import json
import pandas as pd
import requests
from typing import Any, List, TypedDict
from dataclasses import dataclass

API = "http://127.0.0.1:5000/"
API_TODOS = API + "todos/"


class Todoitem(TypedDict):
    """Todo json structure"""

    id: int
    task: str

@dataclass
class TodoList:
    """Todo list json structure"""
    todos: list[Todoitem]


def retrieve_tasks() -> dict:
    """Queries the API and returns the tasks data"""
    url = API_TODOS
    resp = requests.get(url)
    return resp.json()

def add(x,y):
    return x+y

def push_task(task: Todoitem):
    """Pushes a task to the API"""

    if 'id' not in list(task):
        raise ValueError("a Todoitem must have an id")
    
    url = API_TODOS

    resp = requests.post(url, json=task)
    return resp.json()

def process_json(json_input: dict[str, Any]) -> dict:
    """Processes the JSON input"""
    if not json_input:
        raise ValueError("Empty dictionary provided")

    my_dict = json_input

    keys_to_keep = ['name', 'age']
    new_dict = {k: my_dict[k] for k in keys_to_keep}

    return new_dict

def process_dataframe(dataframe_input: pd.DataFrame) -> pd.DataFrame():
    """Processes the dataframe input"""
    if not isinstance(dataframe_input, pd.DataFrame):
        raise TypeError("Parameter is not a pandas DataFrame")    

    if dataframe_input.empty:
        raise ValueError("Empty DataFrame provided")
    
    return dataframe_input[["name", "age", "email"]]

def get_api_body(my_api_response) -> dict:
    return my_api_response.json()