from typing import Any
import pytest
import json
import pandas as pd

from src.utils import process_dataframe, process_json
# from tests/resourses/data_fixtures import dataframe_input_sim_data
# from my_module import process_json, process_dataframe

# Define fixtures for the input objects
@pytest.fixture
def json_input():
    return {
        "name": "John Doe",
        "age": 30,
        "email": "johndoe@example.com",
        "phone": "+1-555-555-5555"
    }

@pytest.fixture
def dataframe_input():
    data = {
        "name": ["John Doe", "Jane Smith"],
        "age": [30, 25],
        "email": ["johndoe@example.com", "janesmith@example.com"],
        "phone": ["+1-555-555-5555", "+1-555-555-1234"]
    }
    return pd.DataFrame(data)

# Define tests for the process_json function
def test_process_json_handles_expected_input_type(json_input: dict[str, Any]):
    assert process_json(json_input) is not None

def test_process_json_raises_error_for_unexpected_input_type():
    with pytest.raises(TypeError):
        process_json("not a dictionary")

def test_process_json_produces_expected_output(json_input: dict[str, Any]):
    expected_output = {"name": "John Doe", "age": 30}
    assert process_json(json_input) == expected_output

def test_process_json_handles_edge_cases():
    with pytest.raises(ValueError):
        process_json({})
    with pytest.raises(ValueError):
        process_json(None) is None

# snapshot is a pytest fixture from snapshottest
## python3 -m pytest --snapshot-update | will update the snapshot and test will pass
def test_process_json_returns_expected_keys_values(snapshot, json_input: dict[str, Any]):
    obj = process_json(json_input)

    keys = list(obj.keys())
    values = list(obj.values())

    snapshot.assert_match(keys, "keys")
    snapshot.assert_match(values, "values")

# Define tests for the process_dataframe function
def test_process_dataframe_handles_expected_input_type(dataframe_input: pd.DataFrame):
    assert process_dataframe(dataframe_input) is not None

def test_process_dataframe_raises_error_for_unexpected_input_type():
    with pytest.raises(TypeError):
        process_dataframe("not a dataframe")

def test_process_dataframe_produces_expected_output(dataframe_input: pd.DataFrame):
    expected_output = pd.DataFrame({
        "name": ["John Doe", "Jane Smith"],
        "age": [30, 25],
        "email": ["johndoe@example.com", "janesmith@example.com"]
    })
    pd.testing.assert_frame_equal(process_dataframe(dataframe_input), expected_output)

def test_process_dataframe_handles_edge_cases():
    with pytest.raises(ValueError):
        process_dataframe(pd.DataFrame())
    with pytest.raises(TypeError):
        process_dataframe(None)

# snapshot is a pytest fixture from snapshottest
## python3 -m pytest --snapshot-update | will update the snapshot and test will pass
def test_process_dataframe_returns_expected_frame(snapshot, dataframe_input: pd.DataFrame): 
    data_frame = process_dataframe(dataframe_input)
    snapshot.assert_match(data_frame.to_csv(index=False), 'unique_data_fixture_name')

# snapshot is a pytest fixture from snapshottest
## python3 -m pytest --snapshot-update | will update the snapshot and test will pass
def test_process_dataframe_returns_expected_cols_dtypes(snapshot, dataframe_input: pd.DataFrame):
    data_frame = process_dataframe(dataframe_input)

    col_names = list(data_frame.columns)
    dtypes = list(data_frame.dtypes)

    snapshot.assert_match(col_names, "col_names")
    snapshot.assert_match(dtypes, "dtypes")
