# Functional testing is done to verify that the software works 
# according to its functional requirements or use cases. 
# It focuses on testing the software from the user's perspective, 
# ensuring that the functionality aligns with the intended use.

import json
import pytest
import responses

from src.utils import API_TODOS, push_task, retrieve_tasks, TodoList, Todoitem
# you can import fixtures if they are reused more than once
from tests.resources.api_fixtures.todo_lists import to_do_list_fixture, to_do_list_plus_one_fixture 


@responses.activate
def test_push_task_expect_extra_item(to_do_list_fixture, to_do_list_plus_one_fixture):
    """Test that the API returns the correct data"""
    # Declare vars
    api_uri = API_TODOS
    task = {"id": 1, "task": "test"}

    # Act or run system
    ## check number of items in todo list
    responses.add(responses.GET, api_uri, json=to_do_list_fixture, status=200)
    initial_number_of_items = len(retrieve_tasks())
    ## add new item to todo list
    responses.add(responses.POST, api_uri, json=task, status=200)
    ## check number of items in todo list after adding new item
    responses.add(responses.GET, api_uri, json=to_do_list_plus_one_fixture, status=200)
    final_number_of_items = len(retrieve_tasks())

    # Assert or verify result
    assert final_number_of_items == initial_number_of_items + 1

@responses.activate
def test_push_task_expect_value_error():
    """Test that the API returns the correct data"""
    # Declare vars
    api_uri = API_TODOS
    task = {"ids": 1, "task": "test"}

    # Act or run system

    ## add new item to todo list
    responses.add(responses.POST, api_uri, json=task, status=200)

    # Assert or verify result
    with pytest.raises(ValueError):
        push_task(Todoitem(**task))
