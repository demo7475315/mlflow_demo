import pytest
import json

@pytest.fixture()
def to_do_list_fixture():
    """Returns the recorded tasks data"""
    with open("tests/resources/api_fixtures/test_todo_items.json", "r") as f:
        data = json.load(f)
    return data

@pytest.fixture()
def to_do_list_plus_one_fixture():
    """Returns the recorded tasks data"""
    with open("tests/resources/api_fixtures/test_todo_items_plus_one.json", "r") as f:
        data = json.load(f)
    return data