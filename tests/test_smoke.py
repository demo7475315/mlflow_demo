# Smoke testing, also known as build verification testing, 
# is a high-level, preliminary test performed to ensure that 
# the most critical functionalities of the software are working 
# as expected. It is often conducted before more in-depth 
# testing to ensure that basic functionalities are not broken.

import pytest
from src.todo_app import app
from src.utils import API_TODOS



@pytest.fixture
def client():
    with app.test_client() as client:
        yield client

def test_flask_app_runs_without_error(client):
    '''A working flask app will have a todo list'''
    # Declare vars 

    # Act / run system
    response = client.get(API_TODOS)
    data = response.json

    actual = response.status_code
    
    # Assert / verify expectation
    assert actual  == 200
