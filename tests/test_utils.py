#Unit testing is done to test individual units or components of the
#software in isolation. It ensures that each unit works as expected, 
# helping to catch errors early in the development process.

from typing import List
import pytest
import pandas as pd

from src.utils import add

def test_add_given_zero_returns_same_number(test_case: int = 5):
    
    # Expectation and fakes [stubs and mocks]
    expectation = test_case

    # Declare vars 
    ADDVALUE = 0

    # Act / run system
    actual = add(test_case, ADDVALUE)

    # Assert / verify expectation
    assert actual == expectation

# Define a fixture to create a sample JSON object
@pytest.fixture
def json_fixture():
    return {
        "name": "John Doe",
        "age": 30,
        "email": "johndoe@example.com",
        "phone": "+1-555-555-5555"
    }

# Define a fixture to create a sample Pandas DataFrame
@pytest.fixture
def df_fixture():
    data = {
        "name": ["John Doe", "Jane Smith", "Bob Johnson"],
        "age": [30, 25, 40],
        "email": ["johndoe@example.com", "janesmith@example.com", "bobjohnson@example.com"],
        "phone": ["+1-555-555-5555", "+1-555-555-1234", "+1-555-555-6789"]
    }
    return pd.DataFrame(data)
