import responses

# from responses import _recorder
import requests
import json
import pytest

from typing import List, TypedDict
from dataclasses import dataclass

from src.utils import API, API_TODOS, Todoitem, TodoList, get_api_body
from tests.resources.api_fixtures.todo_lists import to_do_list_fixture


##############
# responses library can record the API calls and save them to a file
##############

# @_recorder.record(file_path="tests/resources/api_fixtures/test_retrieve_tasks.json")
# def record_tasks():
#     return retrieve_tasks()

# record_tasks()


# @pytest.fixture()
# def to_do_list_fixture():
#     """Returns the recorded tasks data"""
#     with open("tests/resources/api_fixtures/test_todo_items.json", "r") as f:
#         data = json.load(f)
#     return data


@responses.activate
def test_retrieve_tasks(to_do_list_fixture):
    """Test that the API returns the correct data"""
    # Declare vars
    api_uri = API_TODOS

    # Expectation and fakes
    responses.add(responses.GET, api_uri, json=to_do_list_fixture, status=200)
    data_fixture = TodoList([Todoitem(**dict_body) for dict_body in to_do_list_fixture])


    # Act or run system
    data_request_mock = requests.get(api_uri).json()
    request_body = TodoList([Todoitem(**dict_body) for dict_body in data_request_mock])

    # Assert or verify result
    assert data_fixture == request_body

# snapshot is a pytest fixture from snapshottest
## python3 -m pytest --snapshot-update | will update the snapshot and test will pass
@responses.activate
def test_mything(snapshot, to_do_list_fixture:  List[dict]):
    """Testing the API for /me"""
    api_uri = API_TODOS

    # Expectation and fakes
    responses.add(responses.GET, api_uri, json=to_do_list_fixture, status=200)
    
    # Act or run system
    my_api_response = requests.get(api_uri)

    body = get_api_body(my_api_response)

    # Assert or verify result
    snapshot.assert_match(body, 'get_api_body_fixture')
