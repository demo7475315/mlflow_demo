#!/usr/bin/env bash

IAMROLE=$(curl -s 169.254.169.254/latest/meta-data/iam/security-credentials/)
echo "IAM ROLE: ${IAMROLE}"
GITHUB_TOKEN=$(aws --region eu-west-1 ssm get-parameters --with-decryption --names "/data-engineering/github-token" --query "Parameters[0].Value")

GITHUB_BRANCH="master"
JUMO_REPO="ds_flow_libs"

echo "GITHUB Repo:  ${JUMO_REPO}"
echo "GITHUB Branch:  ${GITHUB_BRANCH}"

git clone -b ${GITHUB_BRANCH} https://${GITHUB_TOKEN//\"}@github.com/jumo/${JUMO_REPO}.git
cd ${JUMO_REPO}
python flow_libs/start.py
